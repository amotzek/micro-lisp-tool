(defgpio led 2 digital-output)

(defproc on ()
  (write-gpio led t)
  (after 1000 (off)))

(defproc off ()
  (write-gpio led nil)
  (after 1000 (on)))

(now (on))
