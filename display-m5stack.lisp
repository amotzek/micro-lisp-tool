(setq black (list 0 0 0))

(setq white (list 255 255 255))

(defdisplay display ili9341 m5stack-esp32-basic-core)

(fill display black)

(set-pen-color display white)

(set-position display 0 210)

(pen-down display)

(label display "Franz jagt im verwahrlosten" 24)

(pen-up display)

(set-position display 0 180)

(pen-down display)

(label display "Taxi quer durch Bayern." 24)

(pen-up display)

(set-position display 0 150)

(pen-down display)

(label display "0123456789 sind Zahlen" 24)

(pen-up display)

(set-position display 0 120)

(pen-down display)

(label display "und . , ; : [ ] besonders" 24)

(pen-up display)

(set-position display 230 90)

(pen-down display)

(map-with
  (lambda (i)
    (progn
      (forward display 40)
      (right display 72)))
  (range 0 5))
