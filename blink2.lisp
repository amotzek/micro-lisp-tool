(defgpio led 2 digital-output)

(defproc blink (state)
  (write-gpio led state)
  (after 1000 (blink (not state))))

(now (blink nil))
