(defproc dragon-curve (display n size)
    (when (greater? n 0)
        (dragon-curve display (- n 1) size)
        (right display 90)
        (dragon-curve display (- 1 n) size))
    (when (less? n 0)
        (dragon-curve display (- 0 1 n) size)
        (left display 90)
        (dragon-curve display (+ 1 n) size))
    (when (equal? n 0)
        (forward display size))
    display)

(deffn get-dragon-bounds (n size)
    (get-bounds
        (dragon-curve
            (pen-down (create-display (quote bounds) nil))
            n
            size)))

(defdisplay display ili9341 odroid-go)

(setq width (third (get-bounds display)))

(setq height (fourth (get-bounds display)))

(setq size 10)

(setq black (list 0 0 0))

(setq white (list 255 255 255))

(deffn center-offset (bounds)
    (list
        (floor (- (/ (- width (third bounds)) 2) (first bounds)))
        (floor (- (/ (- height (fourth bounds)) 2) (second bounds)))))

(defproc show-dragon-curve (n offset)
    (pen-up display)
    (fill display black)
    (set-position display (first offset) (second offset))
    (set-heading display 0)
    (set-pen-color display white)
    (pen-down display)
    (dragon-curve display n size))

(deffn iterate-dragon-curves (n)
    (let
        ((bounds (get-dragon-bounds n size)))
        (if
            (or (greater? (third bounds) width)
                (greater? (fourth bounds) height))
            (now (iterate-dragon-curves 1))
            (progn
                (show-dragon-curve n (center-offset bounds))
                (after 60000 (iterate-dragon-curves (+ n 1)))))))

(now (iterate-dragon-curves 1))
